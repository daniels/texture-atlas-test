/*
 * Copyright © 2011 Benjamin Franzke
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <assert.h>
#include <sys/time.h>

#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include <EGL/egl.h>
#include <EGL/eglext.h>

#include <sys/types.h>
#include <unistd.h>

#include <wayland-client.h>

#define MIN(a, b) ((a) < (b) ? (a) : (b))

/* n x n texture atlas */
#define TEXTURE_DIM 2048

#define DRAWS_PER_STEP 30

struct display {
	bool running;
	struct {
		EGLDisplay dpy;
		EGLContext ctx;
		EGLConfig conf;
	} egl;
};

struct geometry {
	int width, height;
};

struct window {
	struct display *display;
	struct geometry geometry, window_size;
	struct {
		GLuint pos_attr;
		GLuint texcoord_attr;
		GLuint tex_id;
		GLuint tex_uni;
		GLuint fbos[2];
		GLuint fbo_tex_id[2];
		GLuint fbo_tex_uni;
	} gl;

	EGLSurface egl_surface;
	uint32_t *tex_data;
	int active_fbo;
};

static const char *vert_shader_text =
	"precision highp float;\n"
	"attribute vec2 in_pos;\n"
	"attribute vec2 in_texcoord;\n"
	"varying vec2 v_texcoord;\n"
	"void main() {\n"
	"  gl_Position = vec4(in_pos, 0.0, 1.0);\n"
	"  v_texcoord = in_texcoord;\n"
	"}\n";

/* The most obnoxious way I could think of to saturate an alpha channel. */
static const char *frag_shader_text =
	"precision highp float;\n"
	"varying vec2 v_texcoord;\n"
	"uniform sampler2D tex;\n"
	"uniform sampler2D tex_fbo;\n"
	"void main() {\n"
	"  float alpha = 0.0;\n"
	"  while (alpha < 1.0) {\n"
	"    float insane_coord = texture2D(tex, vec2(alpha, alpha)).a;\n"
	"    alpha += 0.0001 * texture2D(tex_fbo, vec2(insane_coord, insane_coord)).a;\n"
	"  }\n"
	"  gl_FragColor.rgba = vec4(texture2D(tex, v_texcoord).rgb, alpha);\n"
	"}\n";

static void
init_egl(struct display *display, struct window *window)
{
	PFNEGLGETPLATFORMDISPLAYEXTPROC egl_get_platform_display;
	static const EGLint context_attribs[] = {
		EGL_CONTEXT_CLIENT_VERSION, 2,
		EGL_NONE
	};

	EGLint config_attribs[] = {
		EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
		EGL_RED_SIZE, 8,
		EGL_GREEN_SIZE, 8,
		EGL_BLUE_SIZE, 8,
		EGL_ALPHA_SIZE, 0,
		EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
		EGL_NONE
	};

	EGLint major, minor, n, count, i;
	EGLConfig *configs;
	EGLBoolean ret;

	egl_get_platform_display = (PFNEGLGETPLATFORMDISPLAYEXTPROC)
		eglGetProcAddress("eglGetPlatformDisplayEXT");
	assert(egl_get_platform_display);
	display->egl.dpy =
		egl_get_platform_display(EGL_PLATFORM_WAYLAND_KHR,
					 wl_display_connect(NULL),
					 NULL);
	assert(display->egl.dpy);

	ret = eglInitialize(display->egl.dpy, &major, &minor);
	assert(ret == EGL_TRUE);
	ret = eglBindAPI(EGL_OPENGL_ES_API);
	assert(ret == EGL_TRUE);

	if (!eglGetConfigs(display->egl.dpy, NULL, 0, &count) || count < 1)
		assert(0);

	configs = calloc(count, sizeof *configs);
	assert(configs);

	ret = eglChooseConfig(display->egl.dpy, config_attribs,
			      configs, count, &n);
	assert(ret && n >= 1);

	for (i = 0; i < n; i++) {
		EGLint red_size;
		eglGetConfigAttrib(display->egl.dpy,
				   configs[i], EGL_RED_SIZE, &red_size);
		if (red_size == 8) {
			display->egl.conf = configs[i];
			break;
		}
	}
	free(configs);
	if (display->egl.conf == NULL) {
		fprintf(stderr, "did not find config\n");
		exit(EXIT_FAILURE);
	}

	display->egl.ctx = eglCreateContext(display->egl.dpy,
					    display->egl.conf,
					    EGL_NO_CONTEXT,
					    context_attribs);
	assert(display->egl.ctx);

	ret = eglMakeCurrent(window->display->egl.dpy, EGL_NO_SURFACE,
			     EGL_NO_SURFACE, window->display->egl.ctx);
	assert(ret == EGL_TRUE);
}

static void
fini_egl(struct display *display)
{
	eglTerminate(display->egl.dpy);
	eglReleaseThread();
}

static GLuint
create_shader(struct window *window, const char *source, GLenum shader_type)
{
	GLuint shader;
	GLint status;

	shader = glCreateShader(shader_type);
	assert(shader != 0);

	glShaderSource(shader, 1, (const char **) &source, NULL);
	glCompileShader(shader);

	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
	if (!status) {
		char log[1000];
		GLsizei len;
		glGetShaderInfoLog(shader, 1000, &len, log);
		fprintf(stderr, "Error: compiling %s: %*s\n",
			shader_type == GL_VERTEX_SHADER ? "vertex" : "fragment",
			len, log);
		exit(1);
	}

	return shader;
}

static void
init_gl(struct window *window)
{
	GLuint frag, vert;
	GLuint program;
	GLint status;
	int i;

	fprintf(stderr, "GL vendor %s, renderer %s\n", glGetString(GL_VENDOR), glGetString(GL_RENDERER));

	frag = create_shader(window, frag_shader_text, GL_FRAGMENT_SHADER);
	vert = create_shader(window, vert_shader_text, GL_VERTEX_SHADER);

	program = glCreateProgram();
	glAttachShader(program, frag);
	glAttachShader(program, vert);
	glLinkProgram(program);

	glGetProgramiv(program, GL_LINK_STATUS, &status);
	if (!status) {
		char log[1000];
		GLsizei len;
		glGetProgramInfoLog(program, 1000, &len, log);
		fprintf(stderr, "Error: linking:\n%*s\n", len, log);
		exit(1);
	}

	glUseProgram(program);

	window->gl.pos_attr = 0;
	glBindAttribLocation(program, window->gl.pos_attr, "in_pos");
	window->gl.texcoord_attr = window->gl.pos_attr + 1;
	glBindAttribLocation(program, window->gl.texcoord_attr, "in_texcoord");
	glLinkProgram(program);

	/* Specify the texture and upload some black. */
	window->tex_data = malloc(TEXTURE_DIM * TEXTURE_DIM * 4);
	memset(window->tex_data, 0xffffffff, TEXTURE_DIM * TEXTURE_DIM * 4);
	glActiveTexture(GL_TEXTURE0);
	glGenTextures(1, &window->gl.tex_id);
	glBindTexture(GL_TEXTURE_2D, window->gl.tex_id);
	/* Use NEAREST to avoid bleeding when the edge of the texture samples
	 * from neighbouring pixels. */
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_BGRA_EXT,
		     TEXTURE_DIM, TEXTURE_DIM,
		     0, GL_BGRA_EXT, GL_UNSIGNED_BYTE, window->tex_data);
	window->gl.tex_uni = glGetUniformLocation(program, "tex");
	glUniform1i(window->gl.tex_uni, 0);

	glDisable(GL_BLEND);

	/* Specify two FBOs. */
	glGenFramebuffers(2, window->gl.fbos);
	glGenTextures(2, window->gl.fbo_tex_id);
	window->gl.fbo_tex_uni = glGetUniformLocation(program, "fbo_tex");

	for (i = 0; i < 2; i++) {
		glActiveTexture(GL_TEXTURE1 + i);
		glBindFramebuffer(GL_FRAMEBUFFER, window->gl.fbos[i]);
		glBindTexture(GL_TEXTURE_2D, window->gl.fbo_tex_id[i]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_BGRA_EXT,
			     window->geometry.width, window->geometry.height,
			     0, GL_BGRA_EXT, GL_UNSIGNED_BYTE, NULL);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
				       window->gl.fbos[i], 0);
		assert(glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);
		glClearColor(1.0, 1.0, 1.0, 1.0);
		glClear(GL_COLOR_BUFFER_BIT);
	}
}

static void
redraw(void *data)
{
	struct window *window = data;
	static const GLfloat vertices[] = {
		-1.0, -1.0,
		-1.0,  1.0,
		 1.0,  1.0,
		 1.0, -1.0,
	};
	GLfloat tex_coords[] = {
		0.25, 0.25,
		0.25, 0.75,
		0.75, 0.75,
		0.75, 0.25,
	};
	struct timeval before, after;
	uint64_t delta_ns;
	uint32_t pix;

	glBindFramebuffer(GL_FRAMEBUFFER, window->gl.fbos[window->active_fbo]);
	glUniform1i(window->gl.fbo_tex_uni, window->active_fbo + 1);

	glViewport(0, 0, window->geometry.width, window->geometry.height);

	glClearColor(1.0, 1.0, 1.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT);

	glVertexAttribPointer(window->gl.pos_attr, 2, GL_FLOAT, GL_FALSE, 0, vertices);
	glVertexAttribPointer(window->gl.texcoord_attr, 2, GL_FLOAT, GL_FALSE, 0, tex_coords);
	glEnableVertexAttribArray(window->gl.pos_attr);
	glEnableVertexAttribArray(window->gl.texcoord_attr);

	gettimeofday(&before, NULL);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

	glDisableVertexAttribArray(window->gl.pos_attr);
	glDisableVertexAttribArray(window->gl.texcoord_attr);

	pix = 0xdeadbeefUL;
	glReadPixels(window->geometry.width / 2, window->geometry.height / 2,
	             1, 1,
		     GL_BGRA_EXT, GL_UNSIGNED_BYTE, &pix);
	gettimeofday(&after, NULL);
	delta_ns = (after.tv_sec * 1000) + (after.tv_usec / 1000);
	delta_ns -= (before.tv_sec * 1000) + (before.tv_usec / 1000);
	fprintf(stderr, "draw took %" PRIu64 "ns, read back 0x%" PRIu32 "\n", delta_ns, pix);
	window->active_fbo = !window->active_fbo;
}

int
main(int argc, char **argv)
{
	struct display display = { .running = true };
	struct window window = {
		.display = &display,
		.geometry.width = 1024,
		.geometry.height = 1024,
	};

	init_egl(&display, &window);
	init_gl(&window);

	/* The mainloop here is a little subtle.  Redrawing will cause
	 * EGL to read events so we can just call
	 * wl_display_dispatch_pending() to handle any events that got
	 * queued up as a side effect. */
	while (display.running) {
		redraw(&window);
	}

	fini_egl(&display);

	return 0;
}
