/*
 * Copyright © 2011 Benjamin Franzke
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <assert.h>
#include <sys/time.h>

#include <wayland-client.h>
#include <wayland-egl.h>

#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include <EGL/egl.h>
#include <EGL/eglext.h>

#include "xdg-shell-unstable-v6-client-protocol.h"

#include <sys/types.h>
#include <unistd.h>

#define MIN(a, b) ((a) < (b) ? (a) : (b))

/* n x n texture atlas */
#define SUBTEX_TILE_PIXELS 512
#define SUBTEX_DIM 8
#define SUBTEX_NUM (SUBTEX_DIM * SUBTEX_DIM)
#define SUBTEXF (1.0 / SUBTEX_DIM)

#define DRAWS_PER_STEP 30

struct window;
struct seat;

struct display {
	bool running;
	struct wl_display *display;
	struct wl_registry *registry;
	struct wl_compositor *compositor;
	struct zxdg_shell_v6 *shell;
	struct {
		EGLDisplay dpy;
		EGLContext ctx;
		EGLConfig conf;
	} egl;
};

struct geometry {
	int width, height;
};

struct window {
	struct display *display;
	struct geometry geometry, window_size;
	struct {
		GLuint pos_attr;
		GLuint texcoord_attr;
		GLuint tex_id;
		GLuint tex_uni;
	} gl;

	struct wl_egl_window *native;
	struct wl_surface *surface;
	struct zxdg_surface_v6 *xdg_surface;
	struct zxdg_toplevel_v6 *xdg_toplevel;
	EGLSurface egl_surface;
	struct wl_callback *callback;
	bool wait_for_configure;
	unsigned int redraws;
	uint32_t *tex_data;
};

static const char *vert_shader_text =
	"precision highp float;\n"
	"attribute vec2 in_pos;\n"
	"attribute vec2 in_texcoord;\n"
	"varying vec2 v_texcoord;\n"
	"void main() {\n"
	"  gl_Position = vec4(in_pos, 0.0, 1.0);\n"
	"  v_texcoord = in_texcoord;\n"
	"}\n";

static const char *frag_shader_text =
	"precision highp float;\n"
	"varying vec2 v_texcoord;\n"
	"uniform sampler2D tex;\n"
	"void main() {\n"
	"  gl_FragColor = texture2D(tex, v_texcoord);\n"
	"}\n";

static void
init_egl(struct display *display, struct window *window)
{
	PFNEGLGETPLATFORMDISPLAYEXTPROC egl_get_platform_display;
	static const EGLint context_attribs[] = {
		EGL_CONTEXT_CLIENT_VERSION, 2,
		EGL_NONE
	};

	EGLint config_attribs[] = {
		EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
		EGL_RED_SIZE, 8,
		EGL_GREEN_SIZE, 8,
		EGL_BLUE_SIZE, 8,
		EGL_ALPHA_SIZE, 0,
		EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
		EGL_NONE
	};

	EGLint major, minor, n, count, i;
	EGLConfig *configs;
	EGLBoolean ret;

	egl_get_platform_display = (PFNEGLGETPLATFORMDISPLAYEXTPROC)
		eglGetProcAddress("eglGetPlatformDisplayEXT");
	assert(egl_get_platform_display);
	display->egl.dpy =
		egl_get_platform_display(EGL_PLATFORM_WAYLAND_KHR,
					 display->display,
					 NULL);
	assert(display->egl.dpy);

	ret = eglInitialize(display->egl.dpy, &major, &minor);
	assert(ret == EGL_TRUE);
	ret = eglBindAPI(EGL_OPENGL_ES_API);
	assert(ret == EGL_TRUE);

	if (!eglGetConfigs(display->egl.dpy, NULL, 0, &count) || count < 1)
		assert(0);

	configs = calloc(count, sizeof *configs);
	assert(configs);

	ret = eglChooseConfig(display->egl.dpy, config_attribs,
			      configs, count, &n);
	assert(ret && n >= 1);

	for (i = 0; i < n; i++) {
		EGLint red_size;
		eglGetConfigAttrib(display->egl.dpy,
				   configs[i], EGL_RED_SIZE, &red_size);
		if (red_size == 8) {
			display->egl.conf = configs[i];
			break;
		}
	}
	free(configs);
	if (display->egl.conf == NULL) {
		fprintf(stderr, "did not find config\n");
		exit(EXIT_FAILURE);
	}

	display->egl.ctx = eglCreateContext(display->egl.dpy,
					    display->egl.conf,
					    EGL_NO_CONTEXT,
					    context_attribs);
	assert(display->egl.ctx);
}

static void
fini_egl(struct display *display)
{
	eglTerminate(display->egl.dpy);
	eglReleaseThread();
}

static GLuint
create_shader(struct window *window, const char *source, GLenum shader_type)
{
	GLuint shader;
	GLint status;

	shader = glCreateShader(shader_type);
	assert(shader != 0);

	glShaderSource(shader, 1, (const char **) &source, NULL);
	glCompileShader(shader);

	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
	if (!status) {
		char log[1000];
		GLsizei len;
		glGetShaderInfoLog(shader, 1000, &len, log);
		fprintf(stderr, "Error: compiling %s: %*s\n",
			shader_type == GL_VERTEX_SHADER ? "vertex" : "fragment",
			len, log);
		exit(1);
	}

	return shader;
}

static void
init_gl(struct window *window)
{
	GLuint frag, vert;
	GLuint program;
	GLint status;

	fprintf(stderr, "GL vendor %s, renderer %s\n", glGetString(GL_VENDOR), glGetString(GL_RENDERER));

	frag = create_shader(window, frag_shader_text, GL_FRAGMENT_SHADER);
	vert = create_shader(window, vert_shader_text, GL_VERTEX_SHADER);

	program = glCreateProgram();
	glAttachShader(program, frag);
	glAttachShader(program, vert);
	glLinkProgram(program);

	glGetProgramiv(program, GL_LINK_STATUS, &status);
	if (!status) {
		char log[1000];
		GLsizei len;
		glGetProgramInfoLog(program, 1000, &len, log);
		fprintf(stderr, "Error: linking:\n%*s\n", len, log);
		exit(1);
	}

	glUseProgram(program);

	window->gl.pos_attr = 0;
	glBindAttribLocation(program, window->gl.pos_attr, "in_pos");
	window->gl.texcoord_attr = window->gl.pos_attr + 1;
	glBindAttribLocation(program, window->gl.texcoord_attr, "in_texcoord");
	glLinkProgram(program);

	/* Specify the texture, but do not upload any data to it. */
	glActiveTexture(GL_TEXTURE0);
	glGenTextures(1, &window->gl.tex_id);
	glBindTexture(GL_TEXTURE_2D, window->gl.tex_id);
	/* Use NEAREST to avoid bleeding when the edge of the texture samples
	 * from neighbouring pixels. */
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_BGRA_EXT,
		     SUBTEX_TILE_PIXELS * SUBTEX_DIM,
		     SUBTEX_TILE_PIXELS * SUBTEX_DIM,
		     0, GL_BGRA_EXT, GL_UNSIGNED_BYTE, NULL);
	window->gl.tex_uni = glGetUniformLocation(program, "tex");
	glUniform1i(window->gl.tex_uni, 0);

	/* Allocate enough space for the CPU storage of our tiles.
	 * These are stored linearly (indexed by tile number) on the CPU,
	 * though they are stored tiled on the GPU: this avoids needing to
	 * pass a padded stride to the texture upload. */
	window->tex_data = malloc(SUBTEX_TILE_PIXELS * SUBTEX_TILE_PIXELS * SUBTEX_NUM * 4);
	for (int i = 0; i < SUBTEX_NUM; i++) {
		uint32_t *sub_tex_data = window->tex_data + (i * SUBTEX_TILE_PIXELS * SUBTEX_TILE_PIXELS);
		for (int y = 0; y < SUBTEX_TILE_PIXELS; y++) {
			for (int x = 0; x < SUBTEX_TILE_PIXELS; x++) {
				/* Increase overall brightness with each tile */
				uint8_t ceil = (0xff * (i + 1)) / SUBTEX_NUM;
				/* Increase red along y axis */
				uint8_t red = (ceil * (y + 1)) / SUBTEX_TILE_PIXELS;
				/* Increase green along x axis */
				uint8_t green = (ceil * (x + 1)) / SUBTEX_TILE_PIXELS;
				*sub_tex_data++ = (0xff << 24UL) | (red << 16UL) | (green << 8UL);
			}
		}
	}

	glDisable(GL_BLEND);
}

static void
handle_surface_configure(void *data, struct zxdg_surface_v6 *surface,
			 uint32_t serial)
{
	struct window *window = data;

	zxdg_surface_v6_ack_configure(surface, serial);

	window->wait_for_configure = false;
}

static const struct zxdg_surface_v6_listener xdg_surface_listener = {
	handle_surface_configure
};

static void
handle_toplevel_configure(void *data, struct zxdg_toplevel_v6 *toplevel,
			  int32_t width, int32_t height,
			  struct wl_array *states)
{
	struct window *window = data;
	struct wl_region *region;

	if (width > 0 && height > 0) {
		window->geometry.width = width;
		window->geometry.height = height;
	}

	wl_egl_window_resize(window->native,
			     window->geometry.width,
			     window->geometry.height,
			     0,
			     0);

	region = wl_compositor_create_region(window->display->compositor);
	wl_region_add(region, 0, 0,
		      window->geometry.width,
		      window->geometry.height);
	wl_surface_set_opaque_region(window->surface, region);
	wl_region_destroy(region);
}

static void
handle_toplevel_close(void *data, struct zxdg_toplevel_v6 *xdg_toplevel)
{
	struct window *window = data;

	window->display->running = false;
}

static const struct zxdg_toplevel_v6_listener xdg_toplevel_listener = {
	handle_toplevel_configure,
	handle_toplevel_close,
};

static void
create_surface(struct window *window)
{
	struct display *display = window->display;
	PFNEGLCREATEPLATFORMWINDOWSURFACEEXTPROC egl_create_window_surface;
	EGLBoolean ret;

	window->surface = wl_compositor_create_surface(display->compositor);

	window->native =
		wl_egl_window_create(window->surface,
				     window->geometry.width,
				     window->geometry.height);
	egl_create_window_surface = (PFNEGLCREATEPLATFORMWINDOWSURFACEEXTPROC)
		eglGetProcAddress("eglCreatePlatformWindowSurfaceEXT");
	assert(egl_create_window_surface);
	window->egl_surface =
		egl_create_window_surface(display->egl.dpy,
					  display->egl.conf,
					  window->native,
					  NULL);

	window->xdg_surface = zxdg_shell_v6_get_xdg_surface(display->shell,
							    window->surface);
	zxdg_surface_v6_add_listener(window->xdg_surface,
				     &xdg_surface_listener,
				     window);

	window->xdg_toplevel =
		zxdg_surface_v6_get_toplevel(window->xdg_surface);
	zxdg_toplevel_v6_add_listener(window->xdg_toplevel,
				      &xdg_toplevel_listener, window);

	zxdg_toplevel_v6_set_title(window->xdg_toplevel, "texture-atlas");

	window->wait_for_configure = true;
	wl_surface_commit(window->surface);

	ret = eglMakeCurrent(window->display->egl.dpy, window->egl_surface,
			     window->egl_surface, window->display->egl.ctx);
	assert(ret == EGL_TRUE);
}

static void
destroy_surface(struct window *window)
{
	if (window->callback)
		wl_callback_destroy(window->callback);

	eglDestroySurface(window->display->egl.dpy, window->egl_surface);
	wl_egl_window_destroy(window->native);

	zxdg_toplevel_v6_destroy(window->xdg_toplevel);
	zxdg_surface_v6_destroy(window->xdg_surface);
	wl_surface_destroy(window->surface);
}

static void
redraw(void *data, struct wl_callback *callback, uint32_t time)
{
	struct window *window = data;
	struct display *display = window->display;
	static const GLfloat vertices[] = {
		-1.0, -1.0,
		-1.0,  1.0,
		 1.0,  1.0,
		 1.0, -1.0,
	};
	unsigned int sub_tex = window->redraws / DRAWS_PER_STEP;
	unsigned int sub_tex_x = sub_tex % SUBTEX_DIM;
	unsigned int sub_tex_y = sub_tex / SUBTEX_DIM;
	float left = sub_tex_x * SUBTEXF;
	float right = (sub_tex_x + 1) * SUBTEXF;
	float bottom = sub_tex_y * SUBTEXF;
	float top = (sub_tex_y + 1) * SUBTEXF;
	GLfloat tex_coords[] = {
		left, bottom,
		left, top,
		right, top,
		right, bottom,
	};

	if (window->redraws % DRAWS_PER_STEP == 0) {
		uint32_t *sub_tex_data = window->tex_data + (sub_tex * SUBTEX_TILE_PIXELS * SUBTEX_TILE_PIXELS);
		struct timeval before, after;
		uint64_t delta_ns;

		gettimeofday(&before, NULL);
		glTexSubImage2D(GL_TEXTURE_2D, 0,
		                sub_tex_x * SUBTEX_TILE_PIXELS,
				sub_tex_y * SUBTEX_TILE_PIXELS,
				SUBTEX_TILE_PIXELS,
				SUBTEX_TILE_PIXELS,
			        GL_BGRA_EXT, GL_UNSIGNED_BYTE,
				sub_tex_data);
		gettimeofday(&after, NULL);
		delta_ns = (after.tv_sec * 1000) + (after.tv_usec / 1000);
		delta_ns -= (before.tv_sec * 1000) + (before.tv_usec / 1000);

		fprintf(stderr, "%" PRIu64 "ns to upload step %d: tile (%d,%d), coords (%d,%d) -> (%d, %d), texcoords (%f,%f) -> (%f, %f)\n", delta_ns, sub_tex, sub_tex_x, sub_tex_y, sub_tex_x * SUBTEX_TILE_PIXELS, sub_tex_y * SUBTEX_TILE_PIXELS, (sub_tex_x + 1) * SUBTEX_TILE_PIXELS, (sub_tex_y + 1) * SUBTEX_TILE_PIXELS, left, bottom, right, top);
	}

	assert(window->callback == callback);
	window->callback = NULL;

	if (callback)
		wl_callback_destroy(callback);

	glViewport(0, 0, window->geometry.width, window->geometry.height);

	glClearColor(0.0, 0.0, 0.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT);

	glVertexAttribPointer(window->gl.pos_attr, 2, GL_FLOAT, GL_FALSE, 0, vertices);
	glVertexAttribPointer(window->gl.texcoord_attr, 2, GL_FLOAT, GL_FALSE, 0, tex_coords);
	glEnableVertexAttribArray(window->gl.pos_attr);
	glEnableVertexAttribArray(window->gl.texcoord_attr);

	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

	glDisableVertexAttribArray(window->gl.pos_attr);
	glDisableVertexAttribArray(window->gl.texcoord_attr);

	eglSwapBuffers(display->egl.dpy, window->egl_surface);

	window->redraws = window->redraws + 1;
	if (window->redraws == DRAWS_PER_STEP * SUBTEX_NUM)
		window->display->running = 0;
}

static void
xdg_shell_ping(void *data, struct zxdg_shell_v6 *shell, uint32_t serial)
{
	zxdg_shell_v6_pong(shell, serial);
}

static const struct zxdg_shell_v6_listener xdg_shell_listener = {
	xdg_shell_ping,
};

static void
registry_handle_global(void *data, struct wl_registry *registry,
		       uint32_t name, const char *interface, uint32_t version)
{
	struct display *d = data;

	if (strcmp(interface, "wl_compositor") == 0) {
		d->compositor =
			wl_registry_bind(registry, name,
					 &wl_compositor_interface,
					 MIN(version, 4));
	} else if (strcmp(interface, "zxdg_shell_v6") == 0) {
		d->shell = wl_registry_bind(registry, name,
					    &zxdg_shell_v6_interface, 1);
		zxdg_shell_v6_add_listener(d->shell, &xdg_shell_listener, d);
	}
}

static void
registry_handle_global_remove(void *data, struct wl_registry *registry,
			      uint32_t name)
{
}

static const struct wl_registry_listener registry_listener = {
	registry_handle_global,
	registry_handle_global_remove
};

int
main(int argc, char **argv)
{
	struct display display = { .running = true };
	struct window window = {
		.display = &display,
		.geometry.width = 512,
		.geometry.height = 512,
	};

	display.display = wl_display_connect(NULL);
	assert(display.display);

	display.registry = wl_display_get_registry(display.display);
	wl_registry_add_listener(display.registry,
				 &registry_listener, &display);

	wl_display_roundtrip(display.display);

	init_egl(&display, &window);
	create_surface(&window);
	init_gl(&window);

	/* The mainloop here is a little subtle.  Redrawing will cause
	 * EGL to read events so we can just call
	 * wl_display_dispatch_pending() to handle any events that got
	 * queued up as a side effect. */
	while (display.running) {
		int ret;

		if (window.wait_for_configure) {
			ret = wl_display_dispatch(display.display);
		} else {
			ret = wl_display_dispatch_pending(display.display);
			redraw(&window, NULL, 0);
		}

		if (ret == -1)
			break;
	}

	destroy_surface(&window);
	fini_egl(&display);

	if (display.shell)
		zxdg_shell_v6_destroy(display.shell);

	if (display.compositor)
		wl_compositor_destroy(display.compositor);

	wl_registry_destroy(display.registry);
	wl_display_flush(display.display);
	wl_display_disconnect(display.display);

	return 0;
}
